using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    public class DestroyAfterEffect : MonoBehaviour
    {
        [SerializeField] private GameObject targetToDestroy=null;
        private ParticleSystem playingEffect;
        private IEnumerator Start()
        {
            playingEffect = GetComponent<ParticleSystem>();
            while (playingEffect.IsAlive())
            {
                yield return new WaitForSeconds(0.5f);
            }
            if (targetToDestroy != null)
            {
                Destroy(targetToDestroy);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}