﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace RPG.Stats
{
    public interface IModifierProvider {
        IEnumerable<float> GetAdditiveModifiers(Stat stat);
        IEnumerable<float> GetPercentageModifiers(Stat stat);
    }
}