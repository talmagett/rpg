﻿using RPG.Saving;
using System.Collections;
using UnityEngine;

namespace RPG.Stats
{
    public class Experience : MonoBehaviour ,ISaveable
    {
        [SerializeField] private float experiencePoints = 0;

        public event System.Action onExperienceGained;

        public float ExperiencePoints => experiencePoints;
        public void GainExperience(float experience) {
            experiencePoints += experience;
            onExperienceGained?.Invoke();
        }
        public object CaptureState()
        {
            return experiencePoints;
        }

        public void RestoreState(object state)
        {
            experiencePoints = (float)state;
        }
    }
}