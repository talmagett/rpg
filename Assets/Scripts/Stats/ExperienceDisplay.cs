﻿using UnityEngine;
using TMPro;

namespace RPG.Stats
{
    public class ExperienceDisplay : MonoBehaviour
    {
        private TMP_Text xpText;
        private Experience experience;
        private void Awake()
        {
            experience = GameObject.FindWithTag("Player").GetComponent<Experience>();
            xpText = GetComponent<TMP_Text>();
        }
        private void Update()
        {
            xpText.text= string.Format("{0:0}" , experience.ExperiencePoints);
        }
    }
}