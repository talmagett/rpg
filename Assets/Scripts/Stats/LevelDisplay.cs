﻿using UnityEngine;
using TMPro;

namespace RPG.Stats
{
    public class LevelDisplay : MonoBehaviour
    {
        private TMP_Text xpText;
        private BaseStats stats;
        private void Awake()
        {
            stats = GameObject.FindWithTag("Player").GetComponent<BaseStats>();
            xpText = GetComponent<TMP_Text>();
        }
        private void Update()
        {
            xpText.text= string.Format("{0:0}" , stats.GetLevel);
        }
    }
}