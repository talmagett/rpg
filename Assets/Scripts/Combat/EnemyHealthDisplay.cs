﻿using UnityEngine;
using RPG.Attributes;
using TMPro;

namespace RPG.Combat
{
    public class EnemyHealthDisplay : MonoBehaviour
    {
        private TMP_Text healthText;
        private Fighter fighter;
        private void Awake()
        {
            fighter = GameObject.FindWithTag("Player").GetComponent<Fighter>();
            healthText = GetComponent<TMP_Text>();
        }
        private void Update()
        {
            healthText.text= fighter.Target != null ? string.Format("{0:0}/{1:0}", fighter.Target.GetHealthPoints, fighter.Target.GetMaxHealthPoints): "None";
        }
    }
}