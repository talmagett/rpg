﻿using RPG.Attributes;
using RPG.Combat;
using RPG.Control;
using UnityEngine;

namespace RPG.Core
{
    [RequireComponent(typeof(Health))]
    public class CombatTarget : MonoBehaviour, IRaycastable
    {
        public CursorType GetCursorType()
        {
            return CursorType.Combat;
        }

        public bool HandleRaycast(PlayerController callingController)
        {
            if (callingController.TryGetComponent(out Fighter fighter))
            {
                if (!fighter.CanAttack(gameObject))
                    return false;

                if (Input.GetMouseButton(0))
                {
                    fighter.Attack(gameObject);
                }
                return true;
            }
            return false;
        }
    }
}