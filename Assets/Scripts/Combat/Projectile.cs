﻿using RPG.Attributes;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Combat
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private float speed = 1;
        [SerializeField] private bool isHoming = true;
        [SerializeField] private GameObject hitEffect = null;
        [SerializeField] private float maxLifeTime = 10f;
        [SerializeField] private GameObject[] destroyOnHit = null;
        [SerializeField] private float lifeAfterImpact = 2;

        [SerializeField] private UnityEvent onHit;
        private Health target = null;
        private GameObject instigator = null;
        private float damage = 0;
        private void Start()
        {
            transform.LookAt(GetAimLocation());
        }
        private void Update()
        {
            if (target == null) return;

            if (isHoming && !target.IsDead)
                transform.LookAt(GetAimLocation());

            transform.Translate(speed * Time.deltaTime * Vector3.forward);
        }
        public void SetTarget(GameObject instigator, Health target, float damage)
        {
            this.target = target;
            this.damage = damage;
            this.instigator = instigator;

            Destroy(gameObject, maxLifeTime);
        }
        private Vector3 GetAimLocation()
        {
            CapsuleCollider targetCapsule = target.GetComponent<CapsuleCollider>();
            if (targetCapsule == null)
                return target.transform.position;
            return target.transform.position + Vector3.up * targetCapsule.height / 2;
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out Health health))
            {
                if (health.IsDead) return;
                if (health == target)
                {
                    if (hitEffect != null)
                    {
                        Instantiate(hitEffect, GetAimLocation(), transform.rotation);
                    }
                    target.TakeDamage(instigator, damage);

                    speed = 0;
                    onHit?.Invoke();

                    foreach (var item in destroyOnHit)
                    {
                        Destroy(item);
                    }
                    Destroy(gameObject, lifeAfterImpact);
                }
            }
        }
    }
}