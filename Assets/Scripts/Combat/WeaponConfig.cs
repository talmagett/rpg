﻿using RPG.Attributes;
using UnityEngine;

namespace RPG.Combat
{
    [CreateAssetMenu(fileName = "Weapon", menuName = "Weapons/Make new weapon", order = 0)]
    public class WeaponConfig : ScriptableObject
    {
        [SerializeField] private Weapon weaponPrefab = null;
        [SerializeField] private AnimatorOverrideController animatorOverride = null;

        [SerializeField] private float weaponRange = 2f;
        [SerializeField] private float weaponDamage;
        [SerializeField] private float percentageBonus=0;
        [SerializeField] private bool isRightHanded = true;
        [SerializeField] private Projectile projectile = null;
        public float GetRange => weaponRange;
        public float GetDamage => weaponDamage;
        public float GetPercentageBonus => percentageBonus;
        public bool HasProjectile => projectile != null;
        const string weaponName = "Weapon";
        public void LaunchProjectile(Transform leftHandTransform, Transform rightHandTransform, Health target,GameObject instigator,float calculatedDamage)
        {
            Transform handTransform = isRightHanded ? rightHandTransform : leftHandTransform;
            Projectile projectileInstance = Instantiate(projectile, handTransform.position, Quaternion.identity);
            projectileInstance.SetTarget(instigator, target, calculatedDamage);
        }
        public Weapon Spawn(Transform leftHandTransform, Transform rightHandTransform, Animator animator)
        {
            DestroyOldWeapon(leftHandTransform, rightHandTransform);

            Weapon weapon = null;

            if (weaponPrefab != null)
            {
                Transform handTransform = isRightHanded ? rightHandTransform : leftHandTransform;
                weapon = Instantiate(weaponPrefab, handTransform);
                weapon.gameObject.name = weaponName;
            }
            var overrideController = animator.runtimeAnimatorController as AnimatorOverrideController;
            if (animatorOverride != null)
                animator.runtimeAnimatorController = animatorOverride;
            else if (overrideController     != null)
            {
                animator.runtimeAnimatorController = overrideController.runtimeAnimatorController;
            }
            return weapon;
        }

        private void DestroyOldWeapon(Transform leftHandTransform, Transform rightHandTransform)
        {
            Transform oldWeapon = rightHandTransform.Find(weaponName);
            if (oldWeapon == null)
            {
                oldWeapon = leftHandTransform.Find(weaponName);
            }
            if (oldWeapon == null)
                return;
            oldWeapon.name = "DESTROYING";
            Destroy(oldWeapon.gameObject);
        }
    }
}