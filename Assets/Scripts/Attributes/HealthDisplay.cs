﻿using System.Collections;
using UnityEngine;
using TMPro;
namespace RPG.Attributes
{
    public class HealthDisplay : MonoBehaviour
    {
        private TMP_Text healthText;
        private Health health;
        private void Awake()
        {
            health = GameObject.FindWithTag("Player").GetComponent<Health>();
            healthText = GetComponent<TMP_Text>();
        }
        private void Update()
        {
            healthText.text= string.Format("{0:0}/{1:0}", health.GetHealthPoints,health.GetMaxHealthPoints);
        }
    }
}