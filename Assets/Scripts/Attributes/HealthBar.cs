﻿using UnityEngine;

namespace RPG.Attributes
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] private Health healthComponent = null;
        [SerializeField] private Transform foreground = null;

        [SerializeField] private GameObject rootCanvas = null;
        private void OnEnable()
        {
            healthComponent.onHealthChange += OnHealthChange;
        }
        private void OnDisable()
        {
            healthComponent.onHealthChange -= OnHealthChange;
        }
        private void OnHealthChange()
        {
            if (Mathf.Approximately(healthComponent.GetFraction, 0)|| Mathf.Approximately(healthComponent.GetFraction, 1))
            {
                rootCanvas.SetActive(false);
                return;
            }
            print(healthComponent.GetFraction);
            rootCanvas.SetActive(true);
            foreground.localScale = new Vector3(healthComponent.GetFraction, 1, 1);
        }
    }
}