﻿using GameDevTV.Utils;
using RPG.Core;
using RPG.Saving;
using RPG.Stats;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Attributes
{
    public class Health : MonoBehaviour, ISaveable
    {
        [SerializeField] private float regenerationPercentage = 70;
        [SerializeField] private UnityEvent<float> takeDamage;
        [SerializeField] private UnityEvent onDie;
        [Min(0)]
        private LazyValue<float> healthPoints;
        internal event System.Action onHealthChange;
        public float GetHealthPoints => healthPoints.value;
        public float GetMaxHealthPoints => GetComponent<BaseStats>().GetStat(Stat.Health);
        public float GetPercentage => 100f * GetFraction;
        public float GetFraction => GetHealthPoints / GetMaxHealthPoints;

        private bool isDead;
        internal bool IsDead => isDead;
        private void Awake()
        {
            healthPoints = new LazyValue<float>(() => GetComponent<BaseStats>().GetStat(Stat.Health));
        }
        private void Start()
        {
            healthPoints.ForceInit();
            onHealthChange?.Invoke();
        }
        private void OnEnable()
        {
            GetComponent<BaseStats>().onLevelUp += RegenerateHealth;
        }
        private void OnDisable()
        {
            GetComponent<BaseStats>().onLevelUp -= RegenerateHealth;
        }
        private void RegenerateHealth()
        {
            float regenHealthPoints = GetComponent<BaseStats>().GetStat(Stat.Health) * regenerationPercentage / 100;
            healthPoints.value = Mathf.Max(healthPoints.value, regenHealthPoints);
        }
        public void Heal(float value)
        {
            healthPoints.value = Mathf.Min(healthPoints.value + value, GetMaxHealthPoints);
        }
        public void TakeDamage(GameObject instigator, float damage)
        {
            healthPoints.value = Mathf.Max(healthPoints.value - damage, 0);
            onHealthChange?.Invoke();
            if (healthPoints.value == 0)
            {
                if (isDead) return;
                onDie?.Invoke();
                AwardExperienxe(instigator);
                Die();
            }
            else
            {
                takeDamage?.Invoke(damage);
            }
        }
        private void AwardExperienxe(GameObject instigator)
        {
            if (instigator.TryGetComponent(out Experience experience))
                experience.GainExperience(GetComponent<BaseStats>().GetStat(Stat.ExperienceReward));
        }
        private void Die()
        {
            if (isDead) return;

            isDead = true;
            GetComponent<Animator>().SetTrigger("die");
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }
        public object CaptureState()
        {
            return healthPoints.value;
        }
        public void RestoreState(object state)
        {
            healthPoints.value = (float)state;
            onHealthChange?.Invoke();
            if (healthPoints.value == 0)
                Die();
        }
    }
}