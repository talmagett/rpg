using RPG.Core;
using RPG.Attributes;
using RPG.Saving;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace RPG.Movement
{
    public class Mover : MonoBehaviour, IAction, ISaveable
    {
        [SerializeField] private float maxSpeed = 6;
        [SerializeField] private float maxPathLength = 40;

        private NavMeshAgent agent;
        private Animator animator;
        private ActionScheduler actionScheduler;
        private Health health;
        private void Awake() {
            agent = GetComponent<NavMeshAgent>();
            animator = GetComponent<Animator>();
            actionScheduler = GetComponent<ActionScheduler>();
            health = GetComponent<Health>();
        }
        void Update()
        {
            agent.enabled = !health.IsDead;
            UpdateAnimator();
        }
        public void StartMoveAction(Vector3 destination, float speedFraction)
        {
            actionScheduler.StartAction(this);
            MoveTo(destination, speedFraction);
        }
        private void UpdateAnimator()
        {
            Vector3 velocity = agent.velocity;
            Vector3 localVelocity = transform.InverseTransformDirection(velocity);
            float speed = localVelocity.z;
            animator.SetFloat("forwardSpeed", speed);
        }
        public bool CanMoveTo(Vector3 destination) 
        {
            NavMeshPath path = new NavMeshPath();
            bool hasPath = NavMesh.CalculatePath(transform.position, destination, NavMesh.AllAreas, path);
            if (!hasPath) return false;
            if (path.status != NavMeshPathStatus.PathComplete) return false;
            if (GetPathLenght(path) > maxPathLength) return false;
            return true;
        }
        public void MoveTo(Vector3 point, float speedFraction)
        {
            agent.speed = maxSpeed * Mathf.Clamp01(speedFraction);
            agent.isStopped = false;
            agent.SetDestination(point);
        }
        public void Cancel()
        {
            agent.isStopped = true;
        }


        private float GetPathLenght(NavMeshPath path)
        {
            float totalLength = 0;
            if (path.corners.Length < 2) return totalLength;
            for (int i = 0; i < path.corners.Length - 1; i++)
            {
                totalLength += Vector3.Distance(path.corners[i], path.corners[i + 1]);
            }
            return totalLength;
        }

        [System.Serializable]
        struct MoverSaveData
        {
            public SerializableVector3 position;
            public SerializableVector3 rotation;
        }
        public object CaptureState()
        {
            MoverSaveData data = new MoverSaveData();
            data.position=new SerializableVector3(transform.position);
            data.rotation = new SerializableVector3(transform.eulerAngles);
            return data;
            /*  Alternative method to save
            Dictionary<string, object> data = new Dictionary<string, object>();
            data["position"]= new SerializableVector3(transform.position);
            data["rotation"]= new SerializableVector3(transform.eulerAngles);
            return data;
            */
        }

        public void RestoreState(object state)
        {
            GetComponent<NavMeshAgent>().enabled = false;

            MoverSaveData data = (MoverSaveData)state;
            transform.position= data.position.ToVector();
            transform.eulerAngles= data.rotation.ToVector();

            /*
            Dictionary<string, object> data = (Dictionary<string, object>)state;
            transform.position = ((SerializableVector3)data["position"]).ToVector();
            transform.eulerAngles = ((SerializableVector3)data["rotation"]).ToVector();
            */
            GetComponent<NavMeshAgent>().enabled = true;
        }
    }
}