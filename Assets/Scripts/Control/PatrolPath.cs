﻿using UnityEngine;

namespace RPG.Control
{
    public class PatrolPath : MonoBehaviour
    {
        const float waypointGizmoRadius = 0.2f;
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.white;
            for (int i = 0; i < transform.childCount; i++)
            {
                int j = GetNextIndex(i);
                Gizmos.DrawSphere(GetWayPoint(i), waypointGizmoRadius);
                Gizmos.DrawLine(GetWayPoint(i), GetWayPoint(j));
            }
        }
        public Vector3 GetWayPoint(int i)
        {
            return transform.GetChild(i).position;
        }
        public int GetNextIndex(int i) {
            if (i + 1 == transform.childCount) {
                return 0;
            }
            return i + 1;
        }
    }
}