﻿using UnityEngine;

namespace RPG.UI.DamageText
{
    public class DamageTextSpawner : MonoBehaviour
    {
        [SerializeField] private DamageText damageTextPrefab = null;
        public void Spawn(float damageAmount)
        {
            DamageText damageText = Instantiate(damageTextPrefab, transform);
            damageText.SetDamageText(damageAmount);
            Destroy(damageText.gameObject, 1);
        }
    }
}