﻿using System.Collections;
using UnityEngine;

namespace RPG.UI.DamageText
{
    public class DamageText : MonoBehaviour
    {
        [SerializeField] private TMPro.TMP_Text text;
        public void SetDamageText(float value) => text.text = value.ToString("0");
    }
}