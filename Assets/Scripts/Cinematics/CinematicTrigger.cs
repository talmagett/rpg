using RPG.Saving;
using UnityEngine;
using UnityEngine.Playables;

namespace RPG.Cinematics
{
    public class CinematicTrigger : MonoBehaviour, ISaveable
    {
        private bool triggered;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player") && !triggered)
            {
                GetComponent<PlayableDirector>().Play();
                triggered = true;
            }
        }
        public object CaptureState()
        {
            return triggered;
        }

        public void RestoreState(object state)
        {
            triggered = (bool)state;
        }

    }
}